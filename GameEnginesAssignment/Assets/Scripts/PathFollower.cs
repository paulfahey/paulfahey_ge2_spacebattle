﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollower : SteeringBehaviour {

	public List<Transform> Waypoints = new List<Transform>();
	public Transform currentWaypoint;
	public Transform target;
	public int wayPointIndex;

	Rigidbody rigidbody;

	Vector3 lookDirection;

	public float moveSpeed;
	public float turnSpeed;
	public float maxTilt =  180f;
	public float tiltScale = 30f;
	public float tiltRange = 30f;
	private float rotX = 0.0f;
	public float deltaX = 0.0f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		currentWaypoint = Waypoints [0];
		target = currentWaypoint;
	}
	
	// Update is called once per frame
	void Update () {
		lookDirection = target.position - transform.position;

		Quaternion rot = Quaternion.LookRotation(lookDirection.normalized);
		transform.rotation = Quaternion.Slerp (transform.rotation, rot, Time.deltaTime * turnSpeed);
        transform.Translate(new Vector3 (0, 0, moveSpeed) * Time.deltaTime);
        //rigidbody.AddForce(transform.forward * 800f);

		if (Vector3.Distance (transform.position, currentWaypoint.position) < 3) {
			Arrive();
		}
	}

    public override Vector3 Calculate()
    {
        return boid.SeekForce(target.position);
    }

    void Arrive()
	{
		wayPointIndex++;
		if (wayPointIndex < Waypoints.Count) {
			currentWaypoint = Waypoints [wayPointIndex];
		} else {
			wayPointIndex = 0;
		}
		target = currentWaypoint;
	}

	void OnTriggerEnter(Collider col)
	{
		
	}

}
