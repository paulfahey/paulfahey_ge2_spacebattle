﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAI : MonoBehaviour {

	public enum States { Init, Idle, Patrol, Fight , Flee , Arrive, Chase, Death }
	public States currentState;

	public GameObject target;
	public GameObject bulletPrefab;

    public Vector3 force;
	public Vector3 acceleration;
	public Vector3 velocity;
    public float mass;
	public float maxSpeed;
	public float maxForce;

    //Vectors for sightline
    Vector3 ahead;
    Vector3 ahead2;

	// Use this for initialization
	void Start () {
		//currentState = States.Init;
	}
	
	// Update is called once per frame
	void Update () {
        //Obstacle checks
        ahead = transform.position + (GetComponent<Rigidbody>().velocity.normalized) * 100;
        ahead2 = transform.position + (GetComponent<Rigidbody>().velocity.normalized) * 100 * 0.5f;

    }

    Vector3 CalculateCollisionAvoidance(Vector3 target)
    {
        Vector3 avoidanceForce = transform.position - target;
        avoidanceForce = avoidanceForce.normalized * maxForce;
        return avoidanceForce;
    }

    void OnTriggerEnter(Collider col)
    {
        Vector3 avoidance = CalculateCollisionAvoidance(col.transform.position);
        GetComponent<Rigidbody>().AddForce(avoidance * maxForce, ForceMode.VelocityChange);
    }

	void SetState(States newState)
	{
		currentState = newState;
	}

	void ActState()
	{
		if (currentState == States.Idle) 
		{
			LookForEnemies();
		}
		if (currentState == States.Patrol) 
		{
			
		}
		if (currentState == States.Fight) 
		{

		}
		if(currentState == States.Chase)
		{

		}
		if (currentState == States.Arrive) 
		{

		}
		if (currentState == States.Death) 
		{

		}
	}

	void LookForEnemies()
	{
		Ray ray;
		RaycastHit hit;
		Vector3 dir = transform.TransformDirection (Vector3.forward);

		if (Physics.Raycast (transform.position, dir,out hit)) 
		{
			if (hit.collider.name == "Ship(Clone)") {
				if (hit.collider.tag != this.gameObject.tag) {
					target = hit.collider.gameObject;
					SetState (States.Fight);
				}
			}
			if (hit.collider.tag == "Obstacle") 
			{
				//Add force away from object
			}
		}
	}

	IEnumerator Initialise()
	{
		yield return new WaitForSeconds(2);
		SetState(States.Idle);
	}

	void Shoot()
	{
		GameObject bulletClone = (GameObject)Instantiate(bulletPrefab, target.transform.position, Quaternion.identity);
	}

	//Vector3 Seek()
	//{
	//	Vector3 toTarget = (target.transform.position - transform.position);
	//	toTarget.Normalize();
	//	Vector3 desiredVelocity = toTarget * maxSpeed;
	//	return desiredVelocity - velocity;
	//}


}
