﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationFollow : SteeringBehaviour
{

    public GameObject leader;
    Vector3 destination;
    Vector3 offset;
    Vector3 target;
    Vector3 source;
    Vector3 outputVelocity;
    Vector3 LookDirection;
    ShipAI myAI;
    ShipAI leaderAI;
    public float slowingSpeed = 2f;
    public float turnSpeed = 2f;
    public float moveSpeed = 5f;

    // Use this for initialization
    void Start()
    {
        myAI = GetComponent<ShipAI>();
        leaderAI = leader.GetComponent<ShipAI>();
        offset = leader.transform.position - transform.position;
        offset = Quaternion.Inverse(leader.transform.rotation) * offset;
    }

    // Update is called once per frame
    void Update()
    {
        destination = leader.transform.position;
        source = transform.position;
        target = destination;
        outputVelocity = Formation(source, target);
        LookDirection = target - source;
        Quaternion rotate = Quaternion.LookRotation(LookDirection.normalized);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotate, Time.deltaTime * turnSpeed);
        transform.Translate(new Vector3(0, 0, moveSpeed) * Time.deltaTime);
    }

    public override Vector3 Calculate()
    {
        return boid.SeekForce(target);
    }

    Vector3 Formation(Vector3 source, Vector3 target)
    {
        Vector3 worldTarget = leader.transform.TransformPoint(offset);
        float distance = (worldTarget - (transform.position)).magnitude;
        float time = distance / myAI.maxSpeed;
        Vector3 pursueTargetPos = leader.transform.position + (time * leader.GetComponent<Rigidbody>().velocity);
        Debug.Log(leader.GetComponent<Rigidbody>().velocity);
        return ArriveForce(pursueTargetPos);
    }

    Vector3 ArriveForce(Vector3 targetPos)
    {
        Vector3 toTarget = target - source;
        float distance = toTarget.magnitude;
        float ramped = myAI.maxSpeed * (distance / slowingSpeed);
        float clamped = Mathf.Min(ramped, myAI.maxSpeed);
        Vector3 desired = clamped * target / distance;
        return desired - leader.GetComponent<Rigidbody>().velocity;
    }

}
